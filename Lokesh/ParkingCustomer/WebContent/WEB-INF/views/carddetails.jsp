<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Card Details</title>
<style type="text/css">
.myrow
{
	padding-bottom:20px;
}
</style>
</head>
<body>
<jsp:include page="index.jsp"/>
	<div class="fluid-container" style="width: 600px; padding-top: 50px; margin-left: 400px;">
		<form action="${action}" method="post">
			<div class="row myrow">
				<div class="col-md-4">
					<label>Payment Method *</label>
				</div>
				<div class="col-md-6">
					<select name="card" class="form-control" id="card">
			    		<option value="Debit Card">Debit Card</option>
						<option value="Credit Card">Credit Card</option>
					</select>
				</div>	
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Card Number *</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="cardnumber" placeholder="1111-2222-3333-4444" class="form-control" value="${cardNumber}">
				</div>	
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Account Holder Name *</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="accname" placeholder="Name" class="form-control" value="${accName}">
				</div>	
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Card Expiry Date *</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="carddate" placeholder="MM/YY" class="form-control" id="inputExpDate" maxlength='7' value="${expiryDate}">
				</div>	
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Enter CVV *</label>
				</div>
				<div class="col-md-6">
					<input type="password" class="form-control" value="${cvvNo}" name="cvv" class="cvv" placeholder="CVV">
				</div>	
			</div>
			
			<div>
				<center><input class="btn btn-info" type="submit" value="PAY"/></center>
			</div>
			
			<div>
				<center><h3>${resultMessage}</h3></center>
			</div>
		</form>
	</div>
</body>
</html>