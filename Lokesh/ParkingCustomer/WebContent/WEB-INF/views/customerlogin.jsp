<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.form-container
{
	background: #fff;
	padding: 30px;
	border-radius: 10px;
	box-shadow: 0px 0px 10px 0px #000;
}
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parking Management System</title>
</head>
<body>
	<div class="container-fluid" style="width: 450px; padding-top: 200px;">
	<form class="form-container" action="${action}" method="post">
			<center><h5>Login Form</h5></center>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input name="username" type="text" style="margin-top: 5px;" class="form-control" placeholder="Enter Username" value="${usernameMessage}"/>
		    <small id="emailHelp" style="margin-top: 10px;" class="form-text text-muted"><div class="error">${usernameMessage}</div></small>
		  </div>
		  
	  	 <div class="form-group">
	    	<label for="exampleInputPassword1">Password</label>
	    	<input name="password" type="password" style="margin-top: 5px;" class="form-control" placeholder="Enter Password" value="${passwordMessage}"/>
	    	<small id="emailHelp" style="margin-top: 10px;" class="form-text text-muted"><div class="error">${passwordMessage}</div></small>
	  	 </div>
	  	 
	  	 <button type="submit" style="margin-top: 10px;"class="btn btn-primary">Submit</button>
	  	 <button type="reset" style="margin-top: 10px;" class="btn btn-primary" value="Reset">Reset</button>
	  	 <a href="./signup" style="color: red;">Signup Form</a>
	</form>
 </div> 	
</body>
</html>