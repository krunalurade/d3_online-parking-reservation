<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parking Management System</title>
<style type="text/css">
.myrow
{
	padding-bottom:20px;
}
</style>
</head>
<body>
<jsp:include page="index.jsp"/>
	<div class="container-fluid" style="width: 600px; padding-top: 50px;">
		<center><h3 style="color: green;">Feedback Form</h3></center><br>
		<form action="${action}" method="post">
			<div class="row myrow">
				<div class="col-md-4">
					<label>Enter Name</label>
				</div>
				<div class="col-md-6">
					<input type="text" name="name" class="form-control" value="${name}">
				</div>
			</div>
			<div class="row myrow">
				<div class="col-md-4">
					<label>Enter Email</label>
				</div>
				<div class="col-md-6">
					<input class="form-control" type="text" name="email" value="${email}">
				</div>
			</div>
			<div class="row myrow">
				<div class="col-md-4">
					<label>Message</label>
				</div>
				<div class="col-md-6">
					<textarea rows="5" cols="5" class="form-control">
					</textarea>
				</div>
			</div>
			
			<div>
				<center><input class="btn btn-info" type="submit" value="SUBMIT"/></center>
			</div>
			<center><h3>${resultMessage}</h3></center>
		</form>
	</div>  
</body>
</html>