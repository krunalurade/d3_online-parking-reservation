<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<div class="container-fluid">
	<div class="row" style="background-color: gray;">
		 	<div class="col-md-8">
				<center><div style="height: 70px; padding-top: 20px;"><h3>Parking Management System</h3></div></center>
			</div>
			<div class="col-md-2">
				<div class="login-info" style="margin-top: 20px;">Login : ${loginUserName}</div>
			</div>
			<div class="col-md-2">
				<div class="logout"><a href="./logout" class="btn btn-danger" style="margin-top: 20px;" class="btn btn-danger">LOGOUT</a></div>
			</div>
	</div>
</div>	