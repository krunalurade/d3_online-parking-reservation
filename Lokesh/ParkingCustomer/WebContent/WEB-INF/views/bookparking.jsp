<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
<%@page import="java.util.Date" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parking Management System</title>
<style type="text/css">
.r1
{
	padding-left: 20px;
	width: 100%;
	padding: 30px;
}
.myrow
{
	padding-bottom:10px;
}
.error
{
	color: red;
}
.form-container
{
	background: #fff;
	padding: 30px;
	border-radius: 10px;
	box-shadow: 0px 0px 5px 0px #000;
}
</style>
</head>
<body>
<jsp:include page="index.jsp"/>
	<div class="container-fluid">
			<div class="row r1">
				<div class="col-md-5">
					<form class="form-container" action="${action}" method="post">
					<center><h5>Book Parking</h5></center><br>
						<div class="row myrow">
			 				<div class="col-md-5">Enter Name *</div>
			 					<div class="col-md-6">
			 						<input type="hidden" name="id" value="${id}"/>
			 						<input class="form-control" type="text"	name="name" value="${name}"/>
			 					</div>
<%-- 			 				<div class="col-md-3 error">${phoneError}</div>	 --%>
			 			</div>
			 			
						<div class="row myrow">
			 				<div class="col-md-5">Enter Phone *</div>
			 					<div class="col-md-6">
			 						<input class="form-control" type="text"	name="phone" value="${phone}"/>
			 					</div>
<%-- 			 				<div class="col-md-3 error">${phoneError}</div>	 --%>
			 			</div>
			 			
						<div class="row myrow">
			 				<div class="col-md-5">Enter Email *</div>
			 					<div class="col-md-6">
			 						<input class="form-control" type="text"	name="email" value="${email}"/>
			 					</div>
			 				<div class="col-md-3 error">${emailError}</div>	
			 			</div>
			 			
						<div class="row myrow">
			 				<div class="col-md-5">Select Date *</div>
			 					<div class="col-md-6">
			 						<input class="form-control" type="date"	name="bookingdate" value="${selectDate}"/>
			 					</div>
<%-- 			 				<div class="col-md-3 error">${dateError}</div>	 --%>
			 			</div>

						<div class="row myrow">
			 				<div class="col-md-5">Vechical No *</div>
			 					<div class="col-md-6">
			 						<input type="text" name="vechicalno" class="form-control" id="vechicalno"/>
			 					</div>
			 			</div>
			 						 			
			 			<div class="row myrow">
			 				<div class="col-md-5">Vechicle Type *</div>
			 					<div class="col-md-6">
			 						<select name="vechicaltype" class="form-control" id="vechicaltype">
			    						<option value="2 Wheelers - Rs.50">2 Wheeler - Rs.50</option>
										<option value="4 Wheelers - Rs.100">4 Wheeler - Rs.100</option>
									</select>
			 					</div>
			 			</div>
			 				
						<div class="row myrow">
			 				<div class="col-md-5">Start Time *</div>
			 					<div class="col-md-6">
			 						<select name="bookingtime" class="form-control" id="time">
			    						<option value="01 PM">01:00 PM</option>
										<option value="02 PM">02:00 PM </option>
										<option value="03 PM">03:00 PM</option>
										<option value="04 PM">04:00 PM</option>
										<option value="05 PM">05:00 PM</option>
										<option value="06 PM">06:00 PM</option>
										<option value="07 PM">07:00 PM</option>
										<option value="08 PM">08:00 PM</option>
										<option value="09 PM">09:00 PM</option>
										<option value="10 PM">10:00 PM</option>
										<option value="11 PM">11:00 PM</option>
										<option value="12 PM">12:00 PM</option>
			    					</select>
			 					</div>
			 			</div>
			 			
						<div class="row myrow">
			 				<div class="col-md-5">Start Hours *</div>
			 					<div class="col-md-6">
			 						<select name="bookinghours" class="form-control" id="hours">
			    						<option value="1 hr">1 hr</option>
										<option value="2 hr">2 hr</option>
										<option value="3 hr">3 hr</option>
										<option value="4 hr">4 hr</option>
										<option value="5 hr">5 hr</option>
										<option value="6 hr">6 hr</option>
			    					</select>
			 					</div>
			 			</div>
			 			
			 			
					<div>
	  					<input class="btn btn-primary badge-pill" type="submit" value="${button}"/>
		 				<input class="btn btn-info badge-pill" type="reset" value="RESET"/>
	 				</div>
						
	 			  </form>			   
				</div><!--  -->
				
				<div class="col-md-7">
					<table class="table">
					 	<tr>
							 <th>ID</th>
							 <th>NAME</th>
							 <th>EMAIL</th>
							 <th>PHONE</th>
							 <th>VECHICAL NO</th>
<!-- 							 <th>BOOKING DATE</th> -->
							 <th>TIME</th>
<!-- 							 <th>HOURS</th> -->
							 <th>ACTION</th>
						</tr>
						
					<c:forEach var="customer" items="${customerList}">
					 <tr>
						 <td id="p" value="${customer.id}" name="ids">${customer.id}</td>
						 <td>${customer.name}</td>
						 <td>${customer.email}</td>
						 <td>${customer.phone}</td>
						 <td>${customer.vechicalno}</td>
<%-- 						 <td>${customer.bookingdate}</td> --%>
<%-- 						 <td>${customer.bookigtime}</td> --%>
<%-- 						 <td>${customer.bookinghours}</td> --%>
						 <td>
							 <div class="btn-group">
<%-- 								<a href="./updateCustomer?id=${customer.id}" class="btn btn-success">Update</a> --%>
								<a href="./deleteParking?id=${customer.id}" class="btn btn-danger">Cancel</a>
							 </div>
					 	</td>
					 </tr>
			 </c:forEach>
					</table>
				</div>
			</div>
	</div>	
</body>
</html>

