package org.sunbean.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class DatabaseConnection implements ServletContextListener
{
	private static SessionFactory sf;
	private static Session ses;
	
	public void contextInitialized(ServletContextEvent sce)
	{
	  System.out.println("Context Intialized");
	  
	  sf = new Configuration().configure("/org/sunbean/controller/hibernate.cfg.xml").buildSessionFactory();
	  ses = sf.openSession();
	  
	  System.out.println("Database Configured");
	}
	
	public void contextDestroyed(ServletContextEvent sce)
	{
		System.out.println("Context Destroted");
		
		try 
		{
			if(ses!=null)
			{
				ses.close();
			}
			
			if(sf!=null)
			{
				sf.close();
			}
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
	
	public static Session getDatabaseSession()
	{
		return ses;
	}
}
