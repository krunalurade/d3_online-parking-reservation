package org.sunbean.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.map.HashedMap;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbean.controller.services.CustomerServices;
import org.sunbean.listener.DatabaseConnection;
import org.sunbean.model.Feedbacks;
import org.sunbean.model.Users;

@Controller
public class FeedbackController 
{
	@Autowired
	private CustomerServices customerService;
	
	@RequestMapping(value="/feedBack",method=RequestMethod.POST)
	public ModelAndView getfeedBack(HttpServletRequest req, HttpServletResponse res)
	{
		Map m =new HashedMap();
		
		HttpSession httpSession = req.getSession();
		Users user=(Users)httpSession.getAttribute("UserObject");
		m.put("loginUserName", user.getName());
		
		try
		{
			String name = req.getParameter("name");
			String email = req.getParameter("email");
			String message = req.getParameter("message");
			
			try
			{
				Feedbacks feedback = new Feedbacks();
				
				feedback.setEmail(email);
				feedback.setName(name);
				feedback.setStatus(1);
				feedback.setMessage(message);
				feedback.setUser(user);
				
				Session ses = DatabaseConnection.getDatabaseSession(); 
				ses.beginTransaction();
				ses.save(feedback);
				ses.getTransaction().commit();
//				m.put("resultMessage", "Your Feedback Submitted");
				m.put("action","./bookParkingSlot");
				m.put("button", "SUBMIT");
				return new ModelAndView("bookparking",m);
			}
			catch (Exception ex) 
			{
				m.put("resultMessage", ex.getMessage());
			}
		}
		catch(Exception ex)
		{
			m.put("resultKessage", ex.getMessage());
		}
		return new ModelAndView("feedback",m);
	}
}
