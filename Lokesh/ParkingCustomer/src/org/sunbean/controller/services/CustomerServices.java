package org.sunbean.controller.services;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.sunbean.include.Config;
import org.sunbean.listener.DatabaseConnection;
import org.sunbean.model.Customers;
import org.sunbean.model.Users;

@Service
public class CustomerServices 
{
	public List<Customers> getCustomerList()
	{
		try
		{
			Users user = new Users();
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
			Query query = ses.createQuery("from Customers where status=1 order by id desc"); //user_id='"+user.getId()+"'
			List <Customers> customerList = query.list();
			ses.getTransaction().commit();
			return customerList;
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
//	public List<Customers> getCustomerList()
//	{
//			Session ses = DatabaseConnection.getDatabaseSession();
//			ses.beginTransaction();
//			Query query = ses.createQuery("from Customers where status=1 order by id desc");
//			List <Customers> customerList = query.list();
//			ses.getTransaction().commit();
//			Config.customerHashMap.clear();
//			
//			for(Customers customer: customerList)
//			{
//				Config.customerHashMap.put(customer.getId(), customer);
//			}
//	}
	
	public Customers getCustomer(int id)
	{
		try
		{
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
			Customers customer=(Customers)ses.get(Customers.class, id);	
			ses.getTransaction().commit();
			return customer;	
		}
		catch(Exception ex)
		{
			return null;
		}
	}
	
	public boolean deleteCustomerParking(Customers customer) 
	{
		try
		{
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
			ses.update(customer);
			ses.getTransaction().commit();
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}	
}
