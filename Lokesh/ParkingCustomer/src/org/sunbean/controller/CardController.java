package org.sunbean.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbean.controller.services.CustomerServices;
import org.sunbean.listener.DatabaseConnection;
import org.sunbean.model.Cards;
import org.sunbean.model.Users;

@Controller
public class CardController 
{
	@Autowired
	private CustomerServices customerService;
	
	@RequestMapping(value="/cardDetail", method=RequestMethod.POST)
	public ModelAndView getCardDetails(HttpServletRequest req, HttpServletResponse res)
	{
		Map m = new HashMap();
		
		HttpSession httpSession = req.getSession();
		Users user=(Users)httpSession.getAttribute("UserObject");
		m.put("loginUserName", user.getName());
		
		try
		{
			String cardNumber = req.getParameter("cardnumber");
			String cardDate = req.getParameter("carddate");
			String cvvNo = req.getParameter("cvv");
			String accHolderName = req.getParameter("accname");
			
			try
			{
				Cards card = new Cards();
				card.setAccountHolderName(accHolderName);
				card.setAccountNo(cardNumber);
				card.setCvv(Integer.parseInt(cvvNo));
				card.setExpiryDate(cardDate);
				card.setStatus(1);
				card.setCreated(new Date());
				card.setUser(user);
				
				Session ses = DatabaseConnection.getDatabaseSession();
				ses.beginTransaction();
				ses.save(card);
				ses.getTransaction().commit();
				m.put("resultMessage", "Payment Successful");
				return new ModelAndView("carddetails",m);
			}
			catch(Exception ex)
			{
				m.put("resultMessage", ex.getMessage());
			}
		}
		catch(Exception ex)
		{
			m.put("resultMessage", ex.getMessage());
		}
		return new ModelAndView("carddetails",m);
	}
}
