package org.sunbean.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.sunbean.controller.services.CustomerServices;

@Controller
public class HomeController 
{
	@Autowired
	private CustomerServices customerService;
	
	@RequestMapping(value="/")
	public ModelAndView home()
	{
		Map m =new HashMap();
		m.put("action", "./login");
		return new ModelAndView("customerlogin",m);
	}
	
	@RequestMapping(value="/signup")
	public ModelAndView signUp()
	{
		Map m =new HashMap();
		m.put("action", "./signupcustomer");
		return new ModelAndView("customersignup",m);
	}
	
	@RequestMapping(value="/bookparking")
	public ModelAndView bookslot(HttpServletRequest req,HttpServletResponse res)
	{
		Map m =new HashMap();
		
		m.put("button", "SUBMIT");
		m.put("customerList", customerService.getCustomerList());
		m.put("action", "./bookParkingSlot");
		return new ModelAndView("bookparking",m);
	}
	
	@RequestMapping(value="/feedback")
	public ModelAndView feedback()
	{
		Map m = new HashMap();
		m.put("action", "./feedBack");
		return new ModelAndView("feedback",m);
	}
	
	@RequestMapping(value="/card")
	public ModelAndView card()
	{
		Map m = new HashMap();
		m.put("action", "./cardDetail");
		return new ModelAndView("carddetails",m);
	}
	
	@RequestMapping(value="/home1")
	public ModelAndView homePage()
	{
		return new ModelAndView("home");
	}
	
	@RequestMapping(value="/logout")
	public ModelAndView setLogout(HttpServletRequest req,HttpServletResponse res)
	 {
		 Map m=new HashMap();
		 HttpSession httpSession = req.getSession();
		 httpSession.invalidate();
		 m.put("actionValue", "./login");
		 return new ModelAndView("customerlogin",m);
	 } 
}