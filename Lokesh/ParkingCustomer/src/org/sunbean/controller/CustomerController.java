package org.sunbean.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbean.controller.services.CustomerServices;
import org.sunbean.listener.DatabaseConnection;
import org.sunbean.model.Customers;
import org.sunbean.model.Users;

@Controller
public class CustomerController 
{
//	Users user = new Users();
	SimpleDateFormat bookingDate=new SimpleDateFormat("dd/MM/yyyy");
	
	@Autowired
	private CustomerServices customerService;
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ModelAndView loginCustomer(HttpServletRequest req, HttpServletResponse res)
	{
		Map m = new HashMap();
		HttpSession httpSession = req.getSession();
		
		try
		{
			String username = req.getParameter("username");
			String password = req.getParameter("password");
			
			m.put("username", username);
			
			if(username.isEmpty())
			{
				 m.put("usernameMessage", "Please Enter Username");
			}
			 else if(password.isEmpty())
			 {
				 m.put("passwordMessage", "Please Enter Password");
			 }
			 else
			 {
				 Session ses = DatabaseConnection.getDatabaseSession();
				 ses.beginTransaction();
				 Query query = ses.createQuery("from Users where email='"+username+"' and password='"+password+"' and status=1");
				 
				 List list = query.list();
				 Iterator itr = list.iterator();
				 ses.getTransaction().commit();
				 
				 if(itr.hasNext())
				 {   
					 Users user=(Users)itr.next();
					 httpSession.setAttribute("UserObject", user);
					 httpSession.setAttribute("loginUserName", user.getName());
					 
					 m.put("userLogin", user.getName());
					 m.put("personList",customerService.getCustomerList());
					 return new ModelAndView("index",m);
				 }
				 else
				 {
					 m.put("resultMessage", "Invalid Login");
				 }
			 }
		}
		catch(Exception ex)
		{
			m.put("resultMessage", ex.getMessage());
		}
		return new ModelAndView("customerlogin",m);
	}
	
	@RequestMapping(value="/signupcustomer", method=RequestMethod.POST)
	public ModelAndView signupDetail(HttpServletRequest req,HttpServletResponse res)
	{
		Map m = new HashMap();
		
		try
		{
			String name = req.getParameter("name");
			String username = req.getParameter("username");
			String password = req.getParameter("password");
			
			if(name.isEmpty())
			{
				m.put("nameError", "Please Enter Name");
			}
			else if(username.isEmpty())
			{
				m.put("usernameError", "Please Enter Username");
			}
			else if(password.isEmpty())
			{
				m.put("passwordError", "Please Enter Password");
			}
			else
			{
				try
				{
					Users user = new Users();
					
					user.setName(name);
					user.setEmail(username);
					user.setPassword(password);
					user.setStatus(1);
					user.setCreated(new Date());
					user.setModified(new Date());
					
					Session ses = DatabaseConnection.getDatabaseSession(); 
					ses.beginTransaction();
					
					ses.save(user);
					
					ses.getTransaction().commit();
					
					m.put("action", "./login");
					return new ModelAndView("customerlogin",m);
				}
				catch (Exception ex) 
				{
					m.put("resultMessage", ex.getMessage());
				}
			}	
		}
		catch(Exception ex)
		{
			m.put("resultMessage", ex.getMessage());
		}
		m.put("action", "./signupcustomer");
		return new ModelAndView("customersignup",m);
	}
	
	@RequestMapping(value="/bookParkingSlot", method=RequestMethod.POST)
	public ModelAndView bookParkingSlot(HttpServletRequest req, HttpServletResponse res)
	{
		Map m = new HashMap();
		
		HttpSession httpSession = req.getSession();
		Users user=(Users)httpSession.getAttribute("UserObject");
		m.put("loginUserName", user.getName());
		
		try
		{
			String date = req.getParameter("bookingdate");
			String hours= req.getParameter("bookinghours");
			String time = req.getParameter("bookingtime");
			String name = req.getParameter("name");
			String email = req.getParameter("email");
			String phone = req.getParameter("phone");
			String vechicalNo = req.getParameter("vechicalno");
			String vechicalType = req.getParameter("vechicaltype");
			
			if(name.isEmpty())
			{
				m.put("nameError", "Please Enter Name");
			}
			else if(email.isEmpty())
			{
				m.put("emailError", "Please Enter Email");
			}
			else if(phone.isEmpty())
			{
				m.put("phoneError", "Please Enter Phone");
			}
			else
			{
				try
				{
					Session ses = DatabaseConnection.getDatabaseSession(); 
					ses.beginTransaction();
					Customers customer = new Customers();
					
					customer.setName(name);
					customer.setEmail(email);
					customer.setPhone(phone);
					
					
					customer.setBookdate(new Date());
					customer.setHours(hours);
					customer.setVechicalno(vechicalNo);
					customer.setVehicaltype(vechicalType);
					customer.setBooktime(time);
					customer.setStatus(1);
					customer.setModified(new Date());
					customer.setUser(user);
					
					ses.save(customer);
					
					ses.getTransaction().commit();
//					m.put("resultMessage", "Record Submitted");
					
					m.put("name", "");
					m.put("email", "");
					m.put("phone", "");
					m.put("amount", "");
					m.put("vechicalno", "");
					
					m.put("customerList", customerService.getCustomerList());
					m.put("button", "PAY");
					m.put("action", "./cardDetail");
					return new ModelAndView("carddetails",m);
				}
				catch(Exception ex)
				{
					m.put("resultMessage", ex.getMessage());
				}
			}	
		}
		catch(Exception ex)
		{
			m.put("resultMessage", ex.getMessage());
		}
		return new ModelAndView("bookparing",m);
	}
	
	 @RequestMapping(value="/deleteParking")
	 public ModelAndView getCancelParking(HttpServletRequest req,HttpServletResponse res)
	 {
		 Map m=new HashMap();
		 HttpSession httpSession = req.getSession();
		 Users user=(Users)httpSession.getAttribute("UserObject");
		 m.put("loginUserName", user.getName());
		
		 int id=Integer.parseInt(req.getParameter("id"));
		 Customers customer=customerService.getCustomer(id);
		
		 m.put("id", customer.getId());
		 m.put("name", customer.getName());
		 m.put("phone", customer.getPhone());
		 m.put("email", customer.getEmail());
		 m.put("vechicalno", customer.getVechicalno());
//		 m.put("bookingdate", customer.getBookdate());
//		 m.put("bookingtime", customer.getBooktime()); 
//		 m.put("bookinghours", customer.getHours());
		
		 m.put("customerList", customerService.getCustomerList());
		 m.put("action", "./cancelParking");
		 m.put("button", "CANCEL");
		 return new ModelAndView("bookparking",m);
	 }
	 
	 @RequestMapping(value="/cancelParking")
	 public ModelAndView cancelCustomerParking(HttpServletRequest req,HttpServletResponse res)
	 {
		 Map m=new HashMap();
		 
		 HttpSession httpSession = req.getSession();
		 Users user=(Users)httpSession.getAttribute("UserObject");
		 m.put("loginUserName", user.getName());
		 
		 int id=Integer.parseInt(req.getParameter("id"));
		
		 Customers customer=customerService.getCustomer(id);
		 customer.setStatus(0);
		 
		 boolean result=customerService.deleteCustomerParking(customer);
		
		 if(result)
		 {
			 m.put("resultMessage", "Record Deleted Successfully");
		 }
		 else
		 {
			 m.put("resultMessage", "Could Not Delete Record!");
		 }
		
		 m.put("customerList", customerService.getCustomerList());
		 m.put("action", "./bookParkingSlot");
		 m.put("button", "SUBMIT");
		 return new ModelAndView("bookparking",m);
	 }
	 
}