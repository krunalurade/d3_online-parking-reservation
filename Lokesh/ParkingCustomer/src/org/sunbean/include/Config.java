package org.sunbean.include;

import java.util.LinkedHashMap;
import java.util.Map;

import org.sunbean.model.Cards;
import org.sunbean.model.Customers;
import org.sunbean.model.Feedbacks;
import org.sunbean.model.Users;

public class Config 
{
	public static Map<String, Users> userHashMap = new LinkedHashMap();
	public static Map<String , Customers> customerHashMap = new LinkedHashMap();
	public static Map<String , Feedbacks> feedbackHashMap = new LinkedHashMap();
	public static Map<String , Cards> cardHashMap = new LinkedHashMap();
	
	
	public Users user = null;
}
