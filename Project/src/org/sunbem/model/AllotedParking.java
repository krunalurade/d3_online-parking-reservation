package org.sunbem.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class AllotedParking 
{   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@OneToOne
	private Employee employee;
	@OneToOne
	private Parking parking;
	private int slot2w;
	private int rent2;
	private int slot4w;
	private int rent4;
	
	
	private int status;
	
	@Temporal(TemporalType.DATE)
	private Date created;
	@Temporal(TemporalType.DATE)
	private Date modified;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Parking getParking() {
		return parking;
	}
	public void setParking(Parking parking) {
		this.parking = parking;
	}
	public int getSlot2w() {
		return slot2w;
	}
	public void setSlot2w(int slot2w) {
		this.slot2w = slot2w;
	}
	public int getRent2() {
		return rent2;
	}
	public void setRent2(int rent2) {
		this.rent2 = rent2;
	}
	public int getSlot4w() {
		return slot4w;
	}
	public void setSlot4w(int slot4w) {
		this.slot4w = slot4w;
	}
	public int getRent4() {
		return rent4;
	}
	public void setRent4(int rent4) {
		this.rent4 = rent4;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	
	
}
