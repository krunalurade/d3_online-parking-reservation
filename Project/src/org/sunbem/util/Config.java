package org.sunbem.util;
import java.util.LinkedHashMap;
import java.util.Map;

import org.sunbem.model.AllotedParking;
import org.sunbem.model.Customers;
import org.sunbem.model.Employee;
import org.sunbem.model.Parking;
import org.sunbem.model.Users;

public class Config 
{
	
	public static Customers loginCustomer=null;
	
	public static Map<String,Parking> parkingIdHashMap = new LinkedHashMap();
	public static Map<String, Employee> empIdHashMap = new LinkedHashMap();
	public static Map<String, Employee> empNameHashMap = new LinkedHashMap();
	public static Map<String, Employee> empUserHashMap = new LinkedHashMap();
	public static Map<Integer, Parking> parking2SlotHashMap = new LinkedHashMap();
	public static Map<Integer, Parking> parking4SlotHashMap = new LinkedHashMap();
	
	public static Map<String, Employee> loginEmployee = new LinkedHashMap();
	public static Map<String, AllotedParking> allotedEmailHashMap = new LinkedHashMap();	

	public static Map<String, AllotedParking> allotedEmployeeHashMap = new LinkedHashMap();
	public static Map<String, Customers> customerEmailHashMap = new LinkedHashMap();
	public static Map<Integer, Customers> customerLoginDetailsHashMap = new LinkedHashMap();
	
}
