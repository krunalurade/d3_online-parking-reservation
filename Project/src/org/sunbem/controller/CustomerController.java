package org.sunbem.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CustomerController 
{
	public CustomerController() 
	{
		System.out.println("CustomerController");
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET) //----new  added
	public ModelAndView customerBooking(HttpServletRequest req,HttpServletResponse res,HttpSession httpSession)
	
	{
		 
			    Map m=new HashMap();
		
				httpSession.invalidate();
			    
			    
				m.put("actionValue","customerBooking");
				m.put("buttonValue","LOGIN");
				return new ModelAndView("customerLoginPage",m);	
				
	}

}
