package org.sunbem.controller;

import java.net.Socket;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.controller.services.AllotedParkingServices;
import org.sunbem.controller.services.CustomerServices;
import org.sunbem.controller.services.EmployeeServices;
import org.sunbem.controller.services.ParkingServices;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.AllotedParking;
import org.sunbem.model.Customers;
import org.sunbem.model.Employee;
import org.sunbem.model.Parking;
import org.sunbem.model.Users;
import org.sunbem.util.Config;



@Controller
public class LoginController 
{
	
	@Autowired
	EmployeeServices employeeServices;
	@Autowired
	AllotedParkingServices allotedParkingServices;
	@Autowired
	ParkingServices parkingServices;
	@Autowired
	CustomerServices customerServices;
	
	public LoginController() {

		System.out.println("LoginController");
	}
	
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView home()
	{
		customerServices.getCustomerList();
		
		Map m=new HashMap();
		
		m.put("actionValue","customerBooking");
		m.put("buttonValue","LOGIN");
		return new ModelAndView("customerLoginPage",m);
	}
	
	@RequestMapping(value="/customerBooking",method=RequestMethod.POST) //----new  added
	public ModelAndView customerBooking(HttpServletRequest req,HttpServletResponse res,HttpSession httpSession)
	
	{
		 
			Map m=new HashMap();
		
			String username = req.getParameter("email");
			String password = req.getParameter("password");
			
			Session ses = DatabaseConnection.getDatabaseSession();
			Query query=ses.createQuery("from Customers where email='"+username+"' and password='"+password+"' and status=1");
			List<Customers> list = query.list();
			Iterator<Customers> itr=list.iterator();
			System.out.println("ookol");
			if(itr.hasNext())//username and password are correct
			{
			 
				Customers customer = itr.next();
				 
				
			
					
				 m.put("customerIdValue",customer.getId());
				 
				 httpSession.setAttribute("customerIdValue",customer.getId());
				 
				 m.put("buttonValue","PAYMENT");
				 m.put("parkingList", parkingServices.getParkingList());
			    return new ModelAndView("customerDashboardPage",m);		
				
			}
			else 
			{
				
				m.put("actionValue","customerBooking");
				m.put("buttonValue","LOGIN");
				return new ModelAndView("customerLoginPage",m);		
			}
			
	     }

	
	
	
	@RequestMapping(value="/empLogin",method=RequestMethod.GET)
	public ModelAndView empLogin(HttpServletRequest req,HttpServletResponse res)
	{

		Map m=new HashMap();
		
		m.put("actionValue","employee");
		m.put("buttonValue","LOGIN");
		return new ModelAndView("employeeLoginPage",m);
	}
	
	@RequestMapping(value="/employee",method=RequestMethod.POST) //----new  added
	public ModelAndView employee(HttpServletRequest req,HttpServletResponse res,HttpSession httpSession)
	
	{
			
		allotedParkingServices.getAllotedParkingList();
		
			Map m=new HashMap();
		
			String username = req.getParameter("email");
			String password = req.getParameter("password");
			
			
			Session ses = DatabaseConnection.getDatabaseSession();
			Query query=ses.createQuery("from Employee where email='"+username+"' and password='"+password+"' and status=1");
			List<Employee> list = query.list();
			Iterator<Employee> itr=list.iterator();
			
			
			
			if(itr.hasNext())//username and password are correct
			{
			 
				Employee employee = itr.next();
			
				httpSession.setAttribute("loginUser",employee.getName());
				
				AllotedParking allotedParking = Config.allotedEmailHashMap.get(username);
				
				System.out.println(""+allotedParking.getEmployee().getEmail());
				
				
				String EmpId = allotedParking.getEmployee().getEmpId();
				String name = allotedParking.getEmployee().getName();
				String id = allotedParking.getParking().getParkingId();
				String address = allotedParking.getParking().getAddress();
				int Slot2w = allotedParking.getParking().getSlot2w();
				int Slot4w = allotedParking.getParking().getSlot4w();
				int rent2 = allotedParking.getRent2();
				int rent4 = allotedParking.getRent4();
				
				
				
				
			  m.put("empId", allotedParking.getEmployee().getEmpId());
			  m.put("empName", allotedParking.getEmployee().getName());
			  m.put("parkingId", allotedParking.getParking().getParkingId());
			  m.put("parkingAddress", allotedParking.getParking().getAddress());
			  m.put("cityValue", allotedParking.getParking().getCity());
			  m.put("allottedSlot2Value", allotedParking.getSlot2w());
			  m.put("rent2Value", allotedParking.getRent2());
			  m.put("allottedSlot4Value", allotedParking.getSlot4w());
			  m.put("rent4Value", allotedParking.getRent4());
			  
			  m.put("availableSlot2Value", allotedParking.getParking().getSlot2w());
			  m.put("availableSlot4Value", allotedParking.getParking().getSlot4w());
			  
			  
			  //m.put("msg", "Login User: "+employee.getName());
			  
			  
	          return new ModelAndView("empDashboardPage",m);
		
	       }
			else 
			{
				
				m.put("actionValue","employee");
				m.put("buttonValue","LOGIN");
				return new ModelAndView("login",m);		
			}
			
	     }

	@RequestMapping(value="/adminLogin",method=RequestMethod.GET)
	public ModelAndView adminLogin(HttpServletRequest req,HttpServletResponse res)
	{

		Map m=new HashMap();
		
		m.put("actionValue","adminDashboard");
		m.put("buttonValue","LOGIN");
		return new ModelAndView("adminLoginPage",m);
	}
	
	@RequestMapping(value="/adminDashboard",method=RequestMethod.POST) //----new  added
	public ModelAndView adminDashboard(HttpServletRequest req,HttpServletResponse res)
	
	{

		Map m=new HashMap();
	
		String username = req.getParameter("email");
		String password = req.getParameter("password");
		
		System.out.println("yhgh"+username);
		System.out.println("yhgh"+password);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		Query query=ses.createQuery("from Users where email='"+username+"' and password='"+password+"' and status=1");
		List<Users> list = query.list();
		Iterator<Users> itr=list.iterator();
		
		
		
		if(itr.hasNext())//username and password are correct
		{
		 
			Users user = itr.next();
			
			allotedParkingServices.getAllotedParkingList();
						
			//m.put("msg", "Login User: "+employee.getName());
            return new ModelAndView("dashboardPage",m);
	
       }
		else 
		{
			
			/*m.put("actionValue","employee");*/
			m.put("buttonValue","LOGIN");
			return new ModelAndView("adminLoginPage",m);		
		}
			
			
	}	
	
}
