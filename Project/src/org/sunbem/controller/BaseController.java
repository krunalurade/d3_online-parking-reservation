package org.sunbem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.sunbem.controller.services.EmployeeServices;
import org.sunbem.controller.services.ParkingServices;

public class BaseController 
{
	@Autowired
	protected ParkingServices parkingServices;
	
	@Autowired
	protected EmployeeServices employeeServices;

}
