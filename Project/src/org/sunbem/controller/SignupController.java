package org.sunbem.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.controller.services.EmailServices;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.Customers;

@Controller
public class SignupController 
{
	
	@Autowired
	EmailServices emailService;
	
	public SignupController() {
		System.out.println(" SignupController");
	}

	
	@RequestMapping(value="/addSignup",method=RequestMethod.POST) //----new  added
	public ModelAndView addSignup(HttpServletRequest req,HttpServletResponse res)
	
	{
		
			Map m=new HashMap();
		
			String username = req.getParameter("name");
			String email = req.getParameter("email");
			String phone = req.getParameter("phone");
			String address = req.getParameter("address");
			
			
			int randomNo = new Random().nextInt(10000);
			
			
			Customers customer = new Customers();
			customer.setName(username);
			customer.setEmail(email);
			customer.setPhone(phone);
			customer.setAddress(address);
			customer.setPassword(randomNo+"");
			customer.setCreated(new Date());
			customer.setStatus(1);
			
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
	        ses.save(customer);
	        ses.getTransaction().commit();
	       
	        emailService.sendMail(email, "Your Login Account Password Is: "+randomNo+"", "Online Parking System");
			
	        m.put("actionValue","customerBooking");
			m.put("buttonValue","LOGIN");
		    return new ModelAndView("customerLoginPage",m);		
	     }

	
}
