package org.sunbem.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.Id;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.controller.services.AllotedParkingServices;
import org.sunbem.controller.services.EmployeeServices;
import org.sunbem.controller.services.ParkingServices;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.AllotedParking;
import org.sunbem.model.Customers;

@Controller
public class HomeController 
{
	@Autowired
	private ParkingServices parkingServices;
	@Autowired
	private EmployeeServices employeeServices;
	@Autowired
	private AllotedParkingServices allotedParkingServices;
	
	public HomeController()
	{
		System.out.println("Home Controll");
	}
	
	
	
	
	@RequestMapping(value="/getAddParkingPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getAddParking(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("parkingList",parkingServices.getParkingList());
		
		m.put("actionValue", "addParking");
		m.put("buttonValue", "SUBMIT");
		return new ModelAndView("addParkingPage",m);
	}
	
	@RequestMapping(value="/getSearchParkingPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getSearchParkingPage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("actionValue","searchParkingId");
		m.put("buttonValue","Search");
		
		return new ModelAndView("searchParkingPage",m);
	}
	
	@RequestMapping(value="/getAddEmployeePage",method=RequestMethod.GET) //----new  added
	public ModelAndView getAddEmployeePage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("actionValue", "addEmployee");
		m.put("employeeList", employeeServices.getEmployeeList());
		m.put("buttonValue", "SUBMIT");
		return new ModelAndView("addEmployeePage",m);
	}
	
	@RequestMapping(value="/getSearchEmployeePage",method=RequestMethod.GET) //----new  added
	public ModelAndView getSearchEmployeePage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("actionValue","searchEmployeeId");
		m.put("buttonValue","Search");
		return new ModelAndView("searchEmployeePage",m);
	}
	
	@RequestMapping(value="/getManageParkingPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getParkingAllotementsPage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("employeeList", employeeServices.getEmployeeList());
		m.put("parkingList", parkingServices.getParkingList());
		
		m.put("allotedParkingList", allotedParkingServices.getAllotedParkingList());
		m.put("actionValue","addManageParking");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("manageParkingPage",m);
	}
	
	@RequestMapping(value="/getSignupPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getSignupPage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		m.put("actionValue","/addSignup");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("signupPage",m);
	}
	
	
	@RequestMapping(value="/getPaymentPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getPaymentPage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		//m.put("actionValue","/addSignup");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("customerPaymentPage",m);
	}
	
	
	@RequestMapping(value="/getFeedbackPage",method=RequestMethod.GET) //----new  added
	public ModelAndView getFeedbackPage(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		//m.put("actionValue","/addSignup");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("customerFeedbackPage",m);
	}
	
	@RequestMapping(value="/getBookingPage",method=RequestMethod.GET) //----new  added
	public ModelAndView customerBooking(HttpServletRequest req,HttpServletResponse res)
	
	{
		 
			    Map m=new HashMap();
		
				
				 m.put("buttonValue","PAYMENT");
				 m.put("parkingList", parkingServices.getParkingList());
			    return new ModelAndView("customerDashboardPage",m);		
				
			}
		
}
