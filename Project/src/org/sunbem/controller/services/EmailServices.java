package org.sunbem.controller.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

import org.springframework.stereotype.Service;

@Service
public class EmailServices 
{
	private static String fromEmail;
    
    Map m = new HashMap();
    
    //public void sendMail(String path, MainFrame mainFrame) {
    public void sendMail(String receiver, String text, String subject) {
        String to = receiver;//change accordingly

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return getPasswordAuthonticate();
            }
        });

        //compose message  
        try 
        {
            MimeMessage message = new MimeMessage(session);
            
            message.setFrom(new InternetAddress("onlineparking21@gmail.com"));//change accordingly  
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
            System.out.println("Email Sent Successfully");
        }
        catch (MessagingException e) 
        {
        	System.out.println("Can not send password your email");
            throw new RuntimeException(e);
        }
    }

    private PasswordAuthentication getPasswordAuthonticate() 
    {
    	 return new PasswordAuthentication("onlineparking21@gmail.com","Sunbeam123");//change accordingly@gmail.com  
    }
}
