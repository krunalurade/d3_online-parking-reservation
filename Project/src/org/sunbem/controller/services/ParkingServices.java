package org.sunbem.controller.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.Parking;
import org.sunbem.model.Users;
import org.sunbem.util.Config;

import com.sun.org.apache.regexp.internal.recompile;

@Service
public class ParkingServices 
{
	public ParkingServices() 
	{
		System.out.println("ParkingServices Object created");
	}
	
	public List<Parking> getParkingList()
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from Parking where status=1");
		List<Parking> parkingList = query.list();
		ses.getTransaction().commit();
		
	    Config.parkingIdHashMap.clear();
		for(Parking parking : parkingList)
		{
			Config.parkingIdHashMap.put(parking.getParkingId(), parking);
			Config.parking2SlotHashMap.put(parking.getSlot2w(), parking);
			Config.parking4SlotHashMap.put(parking.getSlot4w(), parking);
	       System.out.println("parking2SlotHashMap"+Config.parking4SlotHashMap.get(parking.getId()));
		}	
		
		return parkingList;
	}
	
	
	
	
	public Parking getParking(int id) //new added
	{
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Parking parking=(Parking)ses.get(Parking.class, id);
		ses.getTransaction().commit();
		return parking;
	}
	
	public Parking getParkingId(String id) 
	{
		Map m =new HashMap();
		
		Session ses = DatabaseConnection.getDatabaseSession();
		Query query = ses.createQuery("from Parking where parkingId='"+id+"' and status =1");
		List<Parking> list = query.list();
		Iterator<Parking> itr = list.iterator();
		
		if (itr.hasNext()) 
		{
			Parking parking = itr.next();			
			return parking;
		}
		else 
		{			
			return null;
		}
	}
	
	public List<Parking> getEmployeeSearchList(String id)
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from Parking WHERE parkingId LIKE '%"+id+"'");   
		List<Parking> parkingList = query.list();
		ses.getTransaction().commit();
		return parkingList;
	}
	

	
	
}
	
	
	