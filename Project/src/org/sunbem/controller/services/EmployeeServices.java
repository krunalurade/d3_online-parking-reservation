package org.sunbem.controller.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.Employee;
import org.sunbem.model.Parking;
import org.sunbem.util.Config;

@Service
public class EmployeeServices 
{
	public EmployeeServices()
	{
		System.out.println("Employee Services Object created");		
	}
	
	public List<Employee> getEmployeeList()
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from Employee where status=1");
		List<Employee> employeeList = query.list();
		ses.getTransaction().commit();
		
	    Config.empIdHashMap.clear();
		for(Employee employee : employeeList)
		{
			Config.empIdHashMap.put(employee.getEmpId(), employee);
			Config.empNameHashMap.put(employee.getName(), employee);
		}	
		
		return employeeList;
	}
	
	
	
	
	public Employee getEmployee(int id) //new added
	{
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Employee employee=(Employee)ses.get(Employee.class, id);
		ses.getTransaction().commit();
		return employee;
	}
	
	public Employee getEmployeeId(String id) 
	{
		Map m =new HashMap();
		
		Session ses = DatabaseConnection.getDatabaseSession();
		Query query = ses.createQuery("from Employee where empId='"+id+"' and status =1");
		List<Employee> list = query.list();
		Iterator<Employee> itr = list.iterator();
		
		if (itr.hasNext()) 
		{
			Employee employee = itr.next();			
			return employee;
		}
		else 
		{			
			return null;
		}
	}
	
	public boolean validateAdmin(String username,String password)
	{ 
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from Uses where email='"+username+"' and password='"+password+"' status=2");
		List<Employee> employeeList = query.list();
		ses.getTransaction().commit();
		if (employeeList.get(0)!=null) 
		{
			return true;
		}
		return false;
	}

}
