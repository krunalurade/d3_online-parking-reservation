package org.sunbem.controller.services;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.Customers;
import org.sunbem.model.Employee;
import org.sunbem.util.Config;

@Service
public class CustomerServices 
{

	public CustomerServices() {
	   System.out.println("CustomerServices");
	}
	
	public List<Customers> getCustomerList()
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from Customers where status=1");
		List<Customers> customerList = query.list();
		ses.getTransaction().commit();
		
	    Config.empIdHashMap.clear();
		for(Customers customer : customerList)
		{
			Config.customerEmailHashMap.put(customer.getEmail(), customer);	
		}	
		System.out.println("hjh"+Config.customerEmailHashMap.values());
		return customerList;
	}
	
	
	public Customers getCustomer(int id) //new added
	{
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Customers customers=(Customers)ses.get(Customers.class, id);
		ses.getTransaction().commit();
		return customers;
	}	
	
}