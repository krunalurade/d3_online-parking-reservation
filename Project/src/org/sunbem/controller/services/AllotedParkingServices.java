package org.sunbem.controller.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.AllotedParking;
import org.sunbem.model.Employee;
import org.sunbem.util.Config;

@Service
public class AllotedParkingServices {

	public AllotedParkingServices()
	{
		System.out.println("AllotedParkingServices Object created");		
	}
	
	public List<AllotedParking> getAllotedParkingList()
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from AllotedParking where status=1");
		List<AllotedParking> allotedParkingList = query.list();
		ses.getTransaction().commit();
		
		System.out.println("allotedParkingList"+allotedParkingList);
		
	    Config.allotedEmailHashMap.clear();
	    
	    for (AllotedParking allotedParking : allotedParkingList) 
	    {
				Config.allotedEmailHashMap.put(allotedParking.getEmployee().getEmail(), allotedParking);
				
				//Config.allotedEmployeeHashMap.put(, arg1);
				
		}
		
	    System.out.println(Config.allotedEmailHashMap.values().size());
		return allotedParkingList;
	}
	
	/*public List<AllotedParking> getAllotedDateList()
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from AllotedParking where status=1");
		List<AllotedParking> allotedDateList = query.list();
		ses.getTransaction().commit();
		
		System.out.println("allotedParkingList"+allotedDateList);
		
	   
	    
		return allotedDateList;
	}
	*/
	
	
	
	
	
	
	
	
	
	
	
	public AllotedParking getAllotedParking (int id) //new added
	{
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		AllotedParking allotedParking = (AllotedParking)ses.get(AllotedParking.class, id);
		ses.getTransaction().commit();
		return allotedParking;
	}
	
	
	public List<AllotedParking> getAllotedEmpParkingList(Employee employee)
	{
		Session ses=DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
		Query query=ses.createQuery("from AllotedParking where Employee_id = '"+employee.getId()+"' and status=1");
		System.out.println();
		List<AllotedParking> allotedEmpParkingList = query.list();
		ses.getTransaction().commit();
		
		System.out.println("allotedEParkingList"+allotedEmpParkingList);
		
	    
	    System.out.println(allotedEmpParkingList.isEmpty());
	     
		return allotedEmpParkingList;
	}
	
	
	
	
	
	
	
	
	
	/*public Employee getEmployeeId(String id) 
	{
		Map m =new HashMap();
		
		Session ses = DatabaseConnection.getDatabaseSession();
		Query query = ses.createQuery("from Employee where empId='"+id+"' and status =1");
		List<Employee> list = query.list();
		Iterator<Employee> itr = list.iterator();
		
		if (itr.hasNext()) 
		{
			Employee employee = itr.next();			
			return employee;
		}
		else 
		{			
			return null;
		}
	}*/
	
}
