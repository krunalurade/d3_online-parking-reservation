package org.sunbem.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.objenesis.instantiator.perc.PercSerializationInstantiator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.controller.services.AllotedParkingServices;
import org.sunbem.controller.services.CustomerServices;
import org.sunbem.controller.services.EmailServices;
import org.sunbem.controller.services.ParkingServices;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.AllotedParking;
import org.sunbem.model.Booking;
import org.sunbem.model.Customers;
import org.sunbem.model.Parking;
import org.sunbem.util.Config;

@RestController
public class ParkingRestController 
{
	@Autowired
	ParkingServices parkingServices;
	
	@Autowired
	AllotedParkingServices allotedParkingServices;
	
	@Autowired
	CustomerServices customerServices;
	
	@Autowired
	EmailServices emailService;
	
	public ParkingRestController() 
	{
		System.out.println("ParkingRestController");
	}
	
	
	
	@RequestMapping(value="/getAvailableSlot",method = RequestMethod.GET)
	public String getAvailableSlot(HttpServletRequest req,HttpServletResponse res)
	{
		String id = req.getParameter("id");
		System.out.println("id :"+id);
		//Parking parking =  parkingServices.getParkingId(id);
	
		
		Parking parking = Config.parkingIdHashMap.get(id);
		
		
		return parking.getSlot2w()+" "+parking.getSlot4w()+" "+parking.getRent2()+" "+parking.getRent4()+" "+parking.getParkingId();
	}
	
	
	
	
	@RequestMapping(value="/get2wheelBook",method=RequestMethod.GET) //----new  added
	public ModelAndView get2wheelBook(HttpServletRequest req,HttpServletResponse res)
	{
		
	try {
			
			
	     
		Map m=new HashMap();
		String slot = req.getParameter("id");
		String id= req.getParameter("id2");
		final int customerId= Integer.parseInt(req.getParameter("id3"));
		
		System.out.println("customerId"+customerId);
		 int slot1   = Integer.parseInt(slot);
		
		slot1 = slot1-1;
		
		
		Parking parking = parkingServices.getParkingId(id);
		Customers customers = customerServices.getCustomer(customerId);
		
		String email = customers.getEmail();
		
		String name =  customers.getName();         
		parking.setSlot2w(slot1);
		
		Booking booking = new Booking();
		booking.setCustomers(customers);
		booking.setName(name);
		booking.setParking(parking);
		booking.setCreated(new Date());
		booking.setModified(new Date());
		booking.setStatus(1);
		
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.update(parking);
        ses.save(booking);
        ses.getTransaction().commit();
		
       // emailService.sendMail(email, "Your 2 Wheeler Booking Conformed", "Online Parking System");
	 	
        m.put("buttonValue","PAYMENT");
		m.put("parkingList", parkingServices.getParkingList());
	    return new ModelAndView("customerDashboardPage",m);	
	    }
	    catch (Exception ex)
		{
		
	    	Map m=new HashMap();
	    	
	    	
	        m.put("buttonValue","PAYMENT");
	 		m.put("parkingList", parkingServices.getParkingList());
	 	    return new ModelAndView("customerDashboardPage",m);	
		}
	    
	}
		
		@RequestMapping(value="/get4wheelBook",method=RequestMethod.GET) //----new  added
		public ModelAndView get4wheelBook(HttpServletRequest req,HttpServletResponse res)
		{
			try {
				
			
			Map m=new HashMap();
			String slot = req.getParameter("id");
			System.out.println("slot :"+slot);
			String id= req.getParameter("id2");
			final int customerId= Integer.parseInt(req.getParameter("id3"));
			
			System.out.println("id :"+id);
			System.out.println("customerId :"+customerId);
			
			 int slot1   = Integer.parseInt(slot);
			
			slot1 = slot1-1;
			
			
			Parking parking = parkingServices.getParkingId(id);
			Customers customers = customerServices.getCustomer(customerId);
			System.out.println("Parking1"+parking.toString());
			parking.setSlot4w(slot1);
			
			
			String email = customers.getEmail();
			
			
			
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
	        ses.update(parking);
	        ses.getTransaction().commit();
	        
	     //   emailService.sendMail(email, "Your 4 Wheeler Booking Conformed", "Online Parking System");
		 	
			
	        m.put("buttonValue","PAYMENT");
			m.put("parkingList", parkingServices.getParkingList());
		    return new ModelAndView("customerDashboardPage",m);	
		    }
		    catch (Exception ex)
			{
			
		    	Map m=new HashMap();
		    	
		    	
		        m.put("buttonValue","PAYMENT");
		 		m.put("parkingList", parkingServices.getParkingList());
		 	    return new ModelAndView("customerDashboardPage",m);	
			}   
	    
	}

	
}

