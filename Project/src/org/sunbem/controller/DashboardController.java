package org.sunbem.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.sunbem.controller.services.AllotedParkingServices;
import org.sunbem.controller.services.EmployeeServices;
import org.sunbem.controller.services.ParkingServices;
import org.sunbem.database.DatabaseConnection;
import org.sunbem.model.AllotedParking;
import org.sunbem.model.Employee;
import org.sunbem.model.Parking;
import org.sunbem.util.Config;

@Controller
public class DashboardController 
{
	@Autowired
	private ParkingServices parkingServices;
	@Autowired
	private EmployeeServices employeeServices;
	@Autowired
	private AllotedParkingServices allotedParkingServices;
	
	Employee employee;
	Parking parking;
	AllotedParking allotedParking;
	
	public DashboardController()
	{
		System.out.println("DashboardController");
	}
	
	
	
	@RequestMapping(value="/addEmployee",method=RequestMethod.POST)
	public ModelAndView addEmployee(HttpServletRequest req,HttpServletResponse res)
	{
		
		Map m =new HashMap();
		System.out.println("addEmployee");
		String empId= req.getParameter("empId");
		String name = req.getParameter("name");
		String email =req.getParameter("emailId");
		String password =req.getParameter("password");
		String address = req.getParameter("address");
		String landmark = req.getParameter("landmark");
		String pin = req.getParameter("pin");
		String city = req.getParameter("city");
		String state = req.getParameter("state");
		String country = req.getParameter("country");
		
		
		Employee employee= new Employee();
		employee.setEmpId(empId);
		employee.setName(name);
		employee.setAddress(address);
		employee.setEmail(email);
		employee.setPassword(password);
		employee.setLandmark(landmark);
		employee.setPin(pin);
		employee.setPin(pin);
		employee.setCity(city);
		employee.setState(state);
		employee.setCountry(country);
		employee.setCreated(new Date());
		employee.setModified(new Date());
		employee.setStatus(1);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.save(employee);
        ses.getTransaction().commit();
        
        m.put("employeeList", employeeServices.getEmployeeList());
        m.put("actionValue", "addEmployee");
		m.put("buttonValue", "SUBMIT");
		return new ModelAndView("addEmployeePage",m);
	}
	
	
	@RequestMapping(value="/getEmployeeUpdateRecord",method=RequestMethod.GET) //----new  added
	public ModelAndView getEmployeeUpdateRecord(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Employee employee = employeeServices.getEmployee(Integer.parseInt(id));
		
		m.put("idValue", employee.getId());
		m.put("empIdValue", employee.getEmpId());
		m.put("empNameValue", employee.getName());
		m.put("addressValue", employee.getAddress());
		m.put("landmarkValue", employee.getLandmark());
		m.put("emailValue", employee.getEmail());
		m.put("passwordValue",employee.getPassword());
		m.put("pinValue", employee.getPin());
		m.put("cityValue", employee.getCity());
		m.put("stateValue", employee.getState());
		m.put("cityValue", employee.getCity());		
		m.put("countryValue", employee.getCountry());			
		m.put("employeeList",employeeServices.getEmployeeList());		
		m.put("actionValue", "UpdatePage");
		m.put("buttonValue","UPDATE");
		
		return new ModelAndView("addEmployeePage",m);
	}
	
	
	@RequestMapping(value="/UpdatePage",method=RequestMethod.POST)
	public ModelAndView UpdatePage(HttpServletRequest req,HttpServletResponse resp) 
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Employee employee = employeeServices.getEmployee(Integer.parseInt(id));
		
		String empId= req.getParameter("empId");
		String name= req.getParameter("name");
		String address = req.getParameter("address");
		String landmark = req.getParameter("landmark");
		String email =req.getParameter("emailId");
		String pin = req.getParameter("pin");
		String city = req.getParameter("city");
		String state = req.getParameter("state");
		String country = req.getParameter("country");
		
		
		employee.setEmpId(empId);
		employee.setName(name);
		employee.setAddress(address);
		employee.setLandmark(landmark);
		employee.setPin(pin);
		employee.setEmail(email);
		employee.setCity(city);
		employee.setState(state);
		employee.setCountry(country);
		employee.setModified(new Date());
		employee.setStatus(1);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.update(employee);
        ses.getTransaction().commit();
       
        m.put("employeeList",employeeServices.getEmployeeList());		
		m.put("actionValue","adminDashboard");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("addEmployeePage",m);
	}
	
	@RequestMapping(value="/getEmployeeDeleteRecord",method=RequestMethod.GET) //----new  added
	public ModelAndView getEmployeeDelete(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Employee employee = employeeServices.getEmployee(Integer.parseInt(id));
		
		m.put("idValue", employee.getId());
		m.put("empIdValue", employee.getEmpId());
		m.put("empNameValue", employee.getName());
		m.put("addressValue", employee.getAddress());
		m.put("landmarkValue", employee.getLandmark());
		m.put("passwordValue",employee.getPassword());
		m.put("emailValue", employee.getEmail());
		m.put("pinValue", employee.getPin());
		m.put("cityValue", employee.getCity());
		m.put("stateValue", employee.getState());
		m.put("cityValue", employee.getCity());		
		m.put("countryValue", employee.getCountry());			
		m.put("employeeList",employeeServices.getEmployeeList());
		m.put("actionValue", "DeletePage");
		m.put("buttonValue","DELETE");
		
		return new ModelAndView("addEmployeePage",m);
	}
	
	
	@RequestMapping(value="/DeletePage",method=RequestMethod.POST)
	public ModelAndView DeletePage(HttpServletRequest req,HttpServletResponse resp) 
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Employee employee = employeeServices.getEmployee(Integer.parseInt(id));
		
		employee.setModified(new Date());
		employee.setStatus(0);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.update(employee);
        ses.getTransaction().commit();
       
        m.put("employeeList",employeeServices.getEmployeeList());		
		m.put("actionValue","adminDashboard");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("addEmployeePage",m);
	}
	
	
	@RequestMapping(value="/addParking",method=RequestMethod.POST)
	public ModelAndView addParking(HttpServletRequest req,HttpServletResponse res)
	{
		
		Map m =new HashMap();
		System.out.println("abc");
		String parkingId = req.getParameter("parkingId");
		int slot2 = Integer.parseInt(req.getParameter("2slot"));
		int rent2 = Integer.parseInt(req.getParameter("2rent"));
		
		int slot4 = Integer.parseInt(req.getParameter("4slot"));
		int rent4 = Integer.parseInt(req.getParameter("4rent"));
		
		
		String address = req.getParameter("address");
		String landmark = req.getParameter("landmark");
		String pin = req.getParameter("pin");
		String city = req.getParameter("city");
		String state = req.getParameter("state");
		String country = req.getParameter("country");
		
		
			Parking parking = new Parking();
			parking.setParkingId(parkingId);
			parking.setSlot2w(slot2);
			parking.setRent2(rent2);
			parking.setSlot4w(slot4);
			parking.setRent4(rent4);
			
			parking.setAddress(address);
			parking.setLandmark(landmark);
			parking.setPin(pin);
			parking.setPin(pin);
			parking.setCity(city);
			parking.setState(state);
			parking.setCountry(country);
			parking.setCreated(new Date());
			parking.setModified(new Date());
			parking.setStatus(1);
			
			Session ses = DatabaseConnection.getDatabaseSession();
			ses.beginTransaction();
	        ses.save(parking);
	        ses.getTransaction().commit();
	        
	        m.put("parkingList",parkingServices.getParkingList());		
			m.put("actionValue","addParking");
			m.put("buttonValue","SUBMIT");
	   	   
		
		    return new ModelAndView("addParkingPage",m);
	}
	
	
	@RequestMapping(value="/getParkingUpdateRecord",method=RequestMethod.GET) //----new  added
	public ModelAndView getParkingUpdateRecord(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Parking parking = parkingServices.getParking(Integer.parseInt(id));
		//2slotValue 2rentValue 4slotValue 4slotValue
		m.put("idValue", parking.getId());
		m.put("parkingIdValue", parking.getParkingId());
		m.put("slot2Value", parking.getSlot2w());
		m.put("charg2Value", parking.getRent2());
		m.put("slot4Value", parking.getSlot4w());
		m.put("charg4Value", parking.getRent4());
		
		m.put("addressValue", parking.getAddress());
		m.put("landmarkValue", parking.getLandmark());
		m.put("pinValue", parking.getPin());
		m.put("cityValue", parking.getCity());
		m.put("stateValue", parking.getState());
		m.put("cityValue", parking.getCity());		
		m.put("countryValue", parking.getCountry());		
		m.put("countryValue", parking.getCountry());		
		m.put("parkingList",parkingServices.getParkingList());		
		m.put("actionValue", "UpdateParkingPage");
		m.put("buttonValue","UPDATE");
		
		
		return new ModelAndView("addParkingPage",m);
	}
	
	
	@RequestMapping(value="/UpdateParkingPage",method=RequestMethod.POST)
	public ModelAndView UpdateParkingPage(HttpServletRequest req,HttpServletResponse resp) 
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Parking parking = parkingServices.getParking(Integer.parseInt(id));
		
		String parkingId = req.getParameter("parkingId");
		String address = req.getParameter("address");
		String landmark = req.getParameter("landmark");
		String pin = req.getParameter("pin");
		String city = req.getParameter("city");
		String state = req.getParameter("state");
		String country = req.getParameter("country");
		int slot2 = Integer.parseInt(req.getParameter("2slot"));
		int rent2 = Integer.parseInt(req.getParameter("2rent"));
		
		int slot4 = Integer.parseInt(req.getParameter("4slot"));
		int rent4 = Integer.parseInt(req.getParameter("4rent"));
		
		parking.setSlot2w(slot2);
		parking.setRent2(rent2);
		parking.setSlot4w(slot4);
		parking.setRent4(rent4);
		parking.setParkingId(parkingId);
		parking.setAddress(address);
		parking.setLandmark(landmark);
		parking.setPin(pin);
		parking.setCity(city);
		parking.setState(state);
		parking.setCountry(country);
		parking.setCreated(new Date());
		parking.setModified(new Date());
		parking.setStatus(1);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.update(parking);
        ses.getTransaction().commit();
       
        m.put("parkingList",parkingServices.getParkingList());		
		m.put("actionValue","addParking");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("addParkingPage",m);
	}
	
	@RequestMapping(value="/getParkingDeleteRecord",method=RequestMethod.GET) //----new  added
	public ModelAndView getPersonDelete(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Parking parking = parkingServices.getParking(Integer.parseInt(id));
		
		m.put("idValue", parking.getId());
		m.put("parkingIdValue", parking.getParkingId());
		m.put("addressValue", parking.getAddress());
		m.put("landmarkValue", parking.getLandmark());
		m.put("pinValue", parking.getPin());
		m.put("cityValue", parking.getCity());
		m.put("stateValue", parking.getState());
		m.put("cityValue", parking.getCity());
		m.put("slot2Value", parking.getSlot2w());
		m.put("charg2Value", parking.getRent2());
		m.put("slot4Value", parking.getSlot4w());
		m.put("charg4Value", parking.getRent4());
		
		
		m.put("countryValue", parking.getCountry());
		
		m.put("parkingList",parkingServices.getParkingList());
		m.put("actionValue", "DeleteParkingPage");
		m.put("buttonValue","DELETE");
		
		return new ModelAndView("addParkingPage",m);
	}
	
	
	
	@RequestMapping(value="/DeleteParkingPage",method=RequestMethod.POST)
	public ModelAndView DeleteParkingPage(HttpServletRequest req,HttpServletResponse resp) 
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		Parking parking = parkingServices.getParking(Integer.parseInt(id));
		parking.setModified(new Date());
		parking.setStatus(0);
		
		Session ses = DatabaseConnection.getDatabaseSession();
		ses.beginTransaction();
        ses.update(parking);
        ses.getTransaction().commit();
       
        m.put("parkingList",parkingServices.getParkingList());		
		m.put("actionValue","addParking");
		m.put("buttonValue","SUBMIT");
		return new ModelAndView("addParkingPage",m);
	}
	
	
	@RequestMapping(value="/searchParkingId",method=RequestMethod.POST) 
	public ModelAndView searchParkingId(HttpServletRequest req,HttpServletResponse res)
	{ 
		   String id = req.getParameter("id");
		
			Map m=new HashMap();
			Parking parking = parkingServices.getParkingId(id);
			
			System.out.println("id"+parking);
			
			m.put("parkingIdValue", parking.getParkingId());
			m.put("addressValue", parking.getAddress());
			m.put("landmarkValue", parking.getLandmark());
			m.put("pinValue", parking.getPin());
			m.put("cityValue", parking.getCity());
			m.put("stateValue", parking.getState());
			m.put("countryValue", parking.getCountry());
			
			m.put("actionValue","searchParkingId");
			m.put("buttonValue","Search");
			
			return new ModelAndView("searchParkingPage",m);
		
	}
	
	
	@RequestMapping(value="/searchEmployeeId",method=RequestMethod.POST) 
	public ModelAndView searchEmployeeId(HttpServletRequest req,HttpServletResponse res)
	{ 
		   String id = req.getParameter("id");
		
			Map m=new HashMap();
			Employee employee = employeeServices.getEmployeeId(id);
			
			System.out.println("id"+employee);
			
			m.put("empId", employee.getEmpId());
			m.put("Name", employee.getName());
			m.put("addressValue", employee.getAddress());
			m.put("landmarkValue", employee.getLandmark());
			m.put("pinValue", employee.getPin());
			m.put("cityValue", employee.getCity());
			m.put("stateValue", employee.getState());
			m.put("countryValue", employee.getCountry());
			
			m.put("actionValue","searchEmployeeId");
			m.put("buttonValue","Search");
			
			return new ModelAndView("searchEmployeePage",m);
		
	}
	
	
	@RequestMapping(value="/addManageParking",method=RequestMethod.POST) 
	public ModelAndView addManageParking(HttpServletRequest req,HttpServletResponse res) throws ParseException
	
		{ 
			 Map m=new HashMap();
			 AllotedParking allotedParking = new AllotedParking();
			
			 
			 String allotedDate= req.getParameter("allotedDate");  
			
			 String employeeId = req.getParameter("employeeId");
			 String parkingId = req.getParameter("parkingId");	
			 int slot2 = Integer.parseInt(req.getParameter("2slot"));
			 int rent2 = Integer.parseInt(req.getParameter("2rent"));
			 int slot4 = Integer.parseInt(req.getParameter("4slot"));
			 int rent4 = Integer.parseInt(req.getParameter("4rent"));
			 
			 
			 
			 allotedParking.setEmployee(Config.empIdHashMap.get(employeeId));
			 allotedParking.setParking(Config.parkingIdHashMap.get(parkingId));
			 allotedParking.setSlot2w(slot2);
			 allotedParking.setRent2(rent2);
			 allotedParking.setSlot4w(slot4);
			 allotedParking.setRent4(rent4);
			 allotedParking.setCreated(new Date());
			 allotedParking.setModified(new Date());
			 allotedParking.setStatus(1);
			 
			 Session ses = DatabaseConnection.getDatabaseSession();
			 ses.beginTransaction();
		     ses.save(allotedParking);
		     ses.getTransaction().commit();
		     
		     
		     m.put("employeeList", employeeServices.getEmployeeList());
		     m.put("parkingList", parkingServices.getParkingList());
				
		     m.put("allotedParkingList", allotedParkingServices.getAllotedParkingList());
			 m.put("actionValue","addManageParking");
			 m.put("buttonValue","SUBMIT");
			
			return new ModelAndView("manageParkingPage",m);
			
		}
	
	@RequestMapping(value="/getAllotedParkingUpdateRecord",method=RequestMethod.GET) //----new  added
	public ModelAndView getAllotedParkingUpdateRecord(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		
		String id = req.getParameter("id");
		System.out.println("id"+id);
		AllotedParking allotedParking = allotedParkingServices.getAllotedParking(Integer.parseInt(id));
		
	
		m.put("empIdValue", allotedParking.getEmployee().getEmpId());
		m.put("parkingIdValue", allotedParking.getParking().getParkingId());
		m.put("addressValue", employee.getAddress());
		m.put("landmarkValue", employee.getLandmark());
		m.put("emailValue", employee.getEmail());
		m.put("pinValue", employee.getPin());
		m.put("cityValue", employee.getCity());
		m.put("stateValue", employee.getState());
		m.put("cityValue", employee.getCity());		
		m.put("countryValue", employee.getCountry());			
		m.put("employeeList",employeeServices.getEmployeeList());		
		m.put("actionValue", "UpdatePage");
		m.put("buttonValue","UPDATE");
		
		return new ModelAndView("addEmployeePage",m);
	}
	
	@RequestMapping(value="/getSearchParking",method=RequestMethod.GET) //----new  added
	public ModelAndView getSearchParking(HttpServletRequest req,HttpServletResponse res)
	{
		Map m=new HashMap();
		String search=req.getParameter("search");
		m.put("parkingList",parkingServices.getEmployeeSearchList(search));
		m.put("actionValue", "addParking");
		m.put("buttonValue", "SUBMIT");
		return new ModelAndView("addParkingPage",m);
	}
	
}
