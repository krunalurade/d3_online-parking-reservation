<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="res/css/style1.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


</head>
<body>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">  
	
	
	
	<div class="col-md-6">
			
			 <div class="row">
					<div class="col-md-4">Employee Id </div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="empId" value="${empId}" >
				    </div>
			</div>  
			
			<div class="row">
					<div class="col-md-4">Employee Name </div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="empId" value="${empName}" >
				    </div>
			</div>  
			
			
				
			 
			<div class="row">
					<div class="col-md-4">Alloted Parking Id </div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingId" value="${parkingId}" >
				    </div>

					   
			</div>  
			
			<div class="row">
					<div class="col-md-4">Alloted Parking Address </div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingAddress" value="${parkingAddress}" >
				    </div>

					   
			</div>  
			  
			<div class="row">
					<div class="col-md-4">City</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingAddress" value="${cityValue}" >
				    </div>

					   
			</div> 
			  
			  
				  
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 2 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot2" name="2slot" value="${allottedSlot2Value}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div>   
					
					<div class="col-md-2">
					   <input type="text" class="form-control" id="rent2" name="2rent" value="${rent2Value}">
				   </div> 
			</div>
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 4 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot4" name="4slot" value="${allottedSlot4Value}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div> 
					 <div class="col-md-2">
					   <input type="text" class="form-control" id="rent4" name="4rent" value="${rent4Value}">
				   </div>    
			</div>
			
			
			
			
	  </div>
	  
	  <div class="col-md-6">
	  			<div class="row">
			 		<div class="col-md-4">Available  Slot 2 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot2" name="2slot" value="${availableSlot2Value}" >					 
					</div>
			</div>
			
			<div class="row">
			 		<div class="col-md-4">Available  Slot 4 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot4" name="4slot" value="${availableSlot4Value}" >					 
					</div>					    
			</div>
	  
	  
	  
	  
      </div>
 
 </div>	






</body>
</html>