<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr">

  <head>
        <jsp:include page="head.jsp" />
 	<script type="text/javascript">
    	function checkfieldforpayment()
    	{
    		if(document.getElementById('card').value == ""){
    			alert('Please Select Card');
    			return false;
    		}
    		else if(document.getElementById('cardno').value == ""){
    			alert('Please Enter Card Number');
    			return false;
    		}
    		else if(document.getElementById('accname').value == ""){
    			alert('Please Account Holder Name');
    			return false;
    		}
    		else if(document.getElementById('expDate').value == ""){
    			alert('Please Enter Expiry Date');
    			return false;
    		}
    		else if(document.getElementById('cvv').value == ""){
    			alert('Please Enter CVV');
    			return false;
    		}
    		else
    		{
    			alert('Your Payment Get Successful');
    			return true;
    		}
    	}
     </script>
  </head>  
  
<body>
  <form action="./${actionValue}" method="post" onsubmit="return checkfieldforpayment()">
   <!--wrapper start-->
        <div class="wrapper">
        
            <!--header menu start-->
            <div class="header">
                <jsp:include page="header.jsp" />
            </div>
            <!--header menu end-->
            
            <!--sidebar start-->
            <div class="sidebar">
                
                  <jsp:include page="sidebarCustomer.jsp" />
                  
            </div>
            <!--sidebar end-->
            
            
            
            <!--main container start-->
            <div class="main-container">
             
                <jsp:include page="customerPayment.jsp" />
              
            </div>
            <!--main container end-->
            
        </div>
        <!--wrapper end-->
  </form>
    
</body>
</html>