        <meta charset="utf-8">
        <title>Dashboard</title>
        <link rel="stylesheet" href="res/css/style1.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" charset="utf-8"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script type="text/javascript">
	$(document).ready(function() {
		
		$("#parkingId").change(function() {
			var id = $("#parkingId").val();

			
			
			$.ajax({
				url : "./getAvailableSlot?id="+id,
				type : "GET",
				contentType : "application/json;charset=utf-8",
				
				success : function(result) {

					var splitted = result.split(" ")
					
					 
					$("#slot2").val(splitted[0]);
					$("#slot4").val(splitted[1]);
					$("#rent2").val(splitted[2]);
					$("#rent4").val(splitted[3]);
					$("#id").val(splitted[4]);
				},
                
				
				error : function(results) {
					alert('No Record Available');
				}
			});
		});


		$("#book2w").click(function() {
			var id = $("#slot2").val();
			var id2 = $("#id").val();
			var id3 = $("#customerId").val();
			
			
			$.ajax({

				
				url : "./get2wheelBook?id="+id+"&id2="+id2+"&id3="+id3,
				type : "GET",
				contentType : "application/json;charset=utf-8",
				
				success : function(result) {

					
					 
					$("#parkingId").result.val();
					
				},
                
				
				error : function(results) {
					alert('No Record Available');
				}
			});
		});


		$("#book4w").click(function() {
			var id = $("#slot4").val();
			var id2 = $("#id").val();
			var id3 = $("#customerId").val();
			
			
			$.ajax({

				
				url : "./get4wheelBook?id="+id+"&id2="+id2+"&id3="+id3,
				type : "GET",
				contentType : "application/json;charset=utf-8",
				
				success : function(result) {

					
					 
					$("#parkingId").result.val();
					
				},
                
				
				error : function(results) {
					alert('No Record Available');
				}
			});
		});

		
		
	});
</script>