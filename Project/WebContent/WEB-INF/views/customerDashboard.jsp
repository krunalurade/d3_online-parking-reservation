<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<a href="./logout"><i class="fas fa-power-off">LogOut</i></a>
<div class="row">  


	<div class="col-md-6">
			 <input type="hidden" class="form-control" id="id" value="${idValue}" >					 
			 <input type="hidden" class="form-control" id="customerId" value="<%=session.getAttribute("customerIdValue")%>" >					 
				
			<div class="row">
					<div class="col-md-4">Select Parking Id </div>
					<div class="col-md-7">
						<select class="form-control" name="parkingId" id="parkingId">
						  <c:if test="${not empty parkingList}">
						    <c:forEach items="${parkingList}" var="parking">
						        <option  value="${parking.parkingId}"  ${parking.parkingId}== selectedParkingId ? 'selected' : ''}> ${parking.parkingId} - - - ${parking.address}</option>
						   	</c:forEach>
						  </c:if> 
						</select> 
					</div>
					   
			</div>  
			
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 2 Wheeler</div>
					<div class="col-md-2">
					<input type="number" class="form-control" id="slot2" name="2slot" value="${availableSlotValue}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div>   
					
					<div class="col-md-2">
					   <input type="text" class="form-control" id="rent2" name="2rent">
				   </div>
				   <div class="col-md-2">
					  <a href="./get2wheelBook" id ="book2w" class="btn btn-info form-control">BOOK</a>
                    </div> 
				    
			</div>
			
			
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 4 Wheeler</div>
					<div class="col-md-2">
					<input type="number" class="form-control" id="slot4" name="4slot" value="${availableSlotValue}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div> 
					 <div class="col-md-2">
					   <input type="text" class="form-control" id="rent4" name="4rent">
				   </div>  
				   <div class="col-md-2">
					  <a href="./get4wheelBook" id ="book4w" class="btn btn-info form-control">BOOK</a>
                    </div>   
			</div>
			
			
			
			
			
			
			
			
			<div class="row">
			 		<div class="col-md-4">
						
					</div>
					<div class="col-md-3">
						 <a href="./getPaymentPage"  class="btn btn-info form-control">PAYMENT</a>
                  </div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
			</div>
	  </div>
	  
	  
	 <div class="col-md-6" >  
                 
                
              <div class="row">
                 
                  
                 </div>
                
            </div>
	
	  
</div>	