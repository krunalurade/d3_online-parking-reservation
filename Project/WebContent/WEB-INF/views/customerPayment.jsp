<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
.myrow
{
	padding-bottom:20px;
}
</style>
<script type="text/javascript">
	function checkforblank()
	{
		if(document.getElementById('card').value == ""){
			alert('Please Select Card');
			return false;
		}
		else if(document.getElementById('cardno').value == ""){
			alert('Please Enter Card Number');
			return false;
		}
		else if(document.getElementById('accname').value == ""){
			alert('Please Account Holder Name');
			return false;
		}
		else if(document.getElementById('expDate').value == ""){
			alert('Please Enter Expiry Date');
			return false;
		}
		else if(document.getElementById('cvv').value == ""){
			alert('Please Enter CVV');
			return false;
		}
		else
		{
			alert('Your Payment Get Successful');
			return true;
		}
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parking Management System</title>
</head>
<body>
	<div style="width: 600px; padding-top: 50px; margin-left: 300px;">
		<div class="row myrow">
					<div class="col-md-4">
						<label>Payment Method *</label>
					</div>
					<div class="col-md-6">
						<select name="card" class="form-control" id="card">
				    		<option value="Debit Card">Debit Card</option>
							<option value="Credit Card">Credit Card</option>
						</select>
					</div>	
				</div>
				
				<div class="row myrow">
					<div class="col-md-4">
						<label>Card Number *</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="cardno" name="cardnumber" placeholder="1111-2222-3333-4444" class="form-control" value="${cardNumber}">
					</div>	
				</div>
				
				<div class="row myrow">
					<div class="col-md-4">
						<label>Account Holder Name *</label>
					</div>
					<div class="col-md-6">
						<input type="text" id="accname" name="accname" placeholder="Name" class="form-control" value="${accName}">
					</div>	
				</div>
				
				<div class="row myrow">
					<div class="col-md-4">
						<label>Card Expiry Date *</label>
					</div>
					<div class="col-md-6">
						<input type="text" name="carddate" placeholder="MM/YY" class="form-control" id="expDate" maxlength='7' value="${expiryDate}">
					</div>	
				</div>
				
				<div class="row myrow">
					<div class="col-md-4">
						<label>Enter CVV *</label>
					</div>
					<div class="col-md-6">
						<input type="password" id="cvv" class="form-control" value="${cvvNo}" name="cvv" class="cvv" placeholder="CVV">
					</div>	
				</div>
				
				<div>
					<center><input class="btn btn-info" type="submit" value="PAY"/></center>
				</div>
		</div>		
</body>
</html>