<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr">
<head>
        <jsp:include page="head.jsp" />

        <script type="text/javascript">
    	function checkforblankfeedback()
    	{
    		if(document.getElementById('nameId').value == ""){
    			alert('Please Enter Name');
    			return false;
    		}
    		else if(document.getElementById('emailId').value == ""){
    			alert('Please Enter Email ID');
    			return false;
    		}
    		else if(document.getElementById('messageId').value == ""){
    			alert('Please Give Some Feedback');
    			return false;
    		}
    		else
    		{
    			alert('Thanks For Your Feedback');
    			return true;
    		}
    	}
        </script>
  </head>  
  
<body>
  <form action="./${actionValue}" method="post" onsubmit="return checkforblankfeedback()">
   <!--wrapper start-->
        <div class="wrapper">
        
            <!--header menu start-->
            <div class="header">
                <jsp:include page="header.jsp" />
            </div>
            <!--header menu end-->
            
            <!--sidebar start-->
            <div class="sidebar">
                
                  <jsp:include page="sidebarCustomer.jsp" />
                  
            </div>
            <!--sidebar end-->
            
            
            
            <!--main container start-->
            <div class="main-container">
                <jsp:include page="customerFeedback.jsp" />
            </div>
            <!--main container end-->
        </div>
        <!--wrapper end-->
  </form>
    
</body>
</html>