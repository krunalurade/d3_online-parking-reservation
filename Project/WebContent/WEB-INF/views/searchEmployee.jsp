<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">
	       <div class="col-md-2">
	       
	       </div>
        
	       <div class="col-md-2">
	         
	         <label for="text">Enter Employee ID:</label>
	      </div>
      
	      <div class="col-md-3">
	        
	          <input type="text" class="form-control" name="id" placeholder="Search here..."/>  
	        
	       </div>
       
	      <div class="col-md-2">
	           <input type="submit" class="btn btn-info" value="${buttonValue}"> 
	      </div>
	      
	      <div class="col-md-1" style="color: green;"><h3>${msg}</h3></div>
	      
     </div>
     
     <div class="row">
	       <div class="col-md-1">
	       
	       </div>
       
	       <div class="col-md-10">
	          <table  class="table">
	              
						<tr>
						   
							<th>EMP ID</th>
							<th>NAME</th>
							<th>ADDRESS</th>
							<th>LANDMARK</th>
							<th>PIN</th>
							<th>CITY</th>
							<th>STATE</th>
							<th>COUNTRY</th>
							
						</tr>
						
												
						
						<tr>
						    <td>${empId}</td>
							<td>${Name}</td>
							<td>${addressValue}</td>
							<td>${landmarkValue}</td>
							<td>${pinValue}</td>
							<td>${cityValue}</td>
							<td>${stateValue}</td>
							<td>${countryValue}</td>
						</tr>	
						
				 </table> 
	      </div>
     </div>
     