<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="ltr">

  <head>
        <jsp:include page="head.jsp" />
  </head>  
  
<body>
  <form action="./${actionValue}" method="post">
   <!--wrapper start-->
        <div class="wrapper">
        
            <!--header menu start-->
            <div class="header">
                <jsp:include page="header.jsp" />
            </div>
            <!--header menu end-->
            
           
            
            
            
            <!--main container start-->
            <div class="main-container">
             
                <jsp:include page="customerlogin.jsp" />
              
            </div>
            <!--main container end-->
            
        </div>
        <!--wrapper end-->


  </form>
    
</body>
</html>