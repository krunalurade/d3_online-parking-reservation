<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">  


	 <div class="col-md-6">
	 	<form action="./${actionValue}" method="post">
	 
	        <input type="hidden" class="form-control" name="id" value="${idValue}" >
			 <div class="row">
				  <div class="col-md-4">Enter Parking ID</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingId" value="${parkingIdValue}" >
				    </div>
			 </div>
			
			
			<div class="row">
                 
                  <div class="col-md-4">Slot for 2 Wheeler</div>
                  
                  <div class="col-md-3">
					   <input type="number" class="form-control" name="2slot" value="${slot2Value}">
				  </div>
                   
                  <div class="col-md-2">Charges Rs:</div>   
                    
                  <div class="col-md-2">
					   <input type="number" class="form-control" name="2rent" value="${charg2Value}">
				   </div>
             </div>
             
             
             <div class="row">
                 
                  <div class="col-md-4">Slot for 4 Wheeler</div>
                  
                  <div class="col-md-3">
					  <input type="number" class="form-control" name="4slot" value="${slot4Value}">
				  </div>
                   
                  <div class="col-md-2">Charges Rs:</div>   
                    
                  <div class="col-md-2">
					   <input type="text" class="form-control" name="4rent" value="${charg4Value}">
				   </div>  
             </div>
             
             <div class="row">
					<div class="col-md-4">Enter Parking Address</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="address" value="${addressValue}" >
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-4">Enter Near Landmark</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="landmark" value="${landmarkValue}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">Enter Area PIN</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="pin" value="${pinValue}">
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-4">Enter City Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="city" value="${cityValue}">
					</div>
				</div>  
					   
				<div class="row">
					<div class="col-md-4">Enter State Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="state" value="${stateValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-4">Enter Country Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="country" value="${countryValue}">
					</div>
				</div>  
				
				<div class="row">
				    <div class="col-md-4">
						
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-info form-control" value="${buttonValue}">
					</div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
				</div>
				</form>
     </div>
    
    
    
    	 <div class="col-md-6" >  
                 <form action="./getSearchParking" method="get">
                 <div class="row">
                 <div class="col-md-7">
                	 <input type="search" class="form-control" name="search" placeholder="Search here...">
                 </div>
                                                                                   
                 <div class="col-md-3">
               		<input type="submit" class="btn btn-info form-control" value="SEARCH">
                 </div>
				 
              </div>
                 
                 
              <div class="row">
                 
                  <table  class="table">
						<tr>
							<th>PARKING ID</th>
							<th>CITY</th>
							<th>STATE</th>
							<th>   ACTION</th>
						</tr>	
						
						<c:if test="${not empty parkingList}">
						<c:forEach var="parking" items="${parkingList}">
						<tr>
							<td>${parking.parkingId}</td>
							<td>${parking.city}</td>
							<td>${parking.state}</td>
						 <td><a href="./getParkingUpdateRecord?id=${parking.id}">UPDATE</a> || <a href="./getParkingDeleteRecord?id=${parking.id}">DELETE</a></td>
							
						</tr>	
						</c:forEach>
						</c:if>
			      </table> 
                 
                 </div>
                </form>
            </div>
     
</div>




<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
      <div class="row">  
         
          <div class="col-md-6">
			<input type="hidden" class="form-control" name="id" value="${idValue}" >
					  
			    <div class="row">
				  <div class="col-md-5">Enter Parking ID</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingId" value="${parkingIdValue}" >
					</div>
				</div>
				
				
		
				<div class="row">
				
				  <div class="col-md-5">Slot for 2 Wheeler</div>		
					<div class="col-md-3">
					  <select class="form-control" name="employeeId">
							<c:forEach begin="1" end="2000" var="counter">
								<option value="">${counter}</option>
							</c:forEach>
					  </select>
					 </div>
					
					<div class="col-md-2">Charges :</div>	
					
					<div class="col-md-2">
					   <input type="text" class="form-control" name="rent" value="${2rent}" >
				    </div>	
				</div>
			
			
			
				<div class="row">
					<div class="col-md-5">Enter Parking Address</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="address" value="${addressValue}" >
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-5">Enter Near Landmark</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="landmark" value="${landmarkValue}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-5">Enter Area PIN</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="pin" value="${pinValue}">
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-5">Enter City Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="city" value="${cityValue}">
					</div>
				</div>  
					   
				<div class="row">
					<div class="col-md-5">Enter State Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="state" value="${stateValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-5">Enter Country Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="country" value="${countryValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-5">Select Employee </div>
					<div class="col-md-7">
						<select class="form-control" name="employeeId">
						  <c:if test="${not empty employeeList}">
						    <c:forEach items="${employeeList}" var="employee">
						        <option value="${employee.empId}"  ${employee.empId}== selectedCity ? 'selected' : ''}> ${employee.empId}  ${employee.name}</option>
						    </c:forEach>
						  </c:if> 
						</select>
					</div>
				</div>  
				
				
				
					   
				<div class="row">
				    <div class="col-md-5">
						
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-info form-control" value="${buttonValue}">
					</div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
				</div>
			
				<div class="row">
					<div class="col-md-6">
						<h1>${msg}</h1>				
					</div>
				</div>	
			  </div>
			  
			  
			  
			  <div class="col-md-1">
			  	<!-- Gap Between two colomn -->
			  </div>
			  
			 
			  
			  <div class="col-md-5" >  
                 
                 <div class="row">
                 <div class="col-md-7">
                	 <input type="search" class="form-control" name="search" placeholder="Search here...">
                 </div>
                                                                                   
                 <div class="col-md-3">
               		<input type="submit" class="btn btn-info form-control" value="SEARCH">
                 </div>
				 
                 </div>
                 
                 
                 <div class="row">
                 
                  <table  class="table">
						<tr>
							<th>PARKING ID</th>
							<th>CITY</th>
							<th>STATE</th>
							<th>   ACTION</th>
						</tr>	
						
						<c:if test="${not empty parkingList}">
						<c:forEach var="parking" items="${parkingList}">
						<tr>
							<td>${parking.parkingId}</td>
							<td>${parking.city}</td>
							<td>${parking.getEmployee().getName()}</td>
						 <td><a href="./getParkingUpdateRecord?id=${parking.id}">UPDATE</a> || <a href="./getParkingDeleteRecord?id=${parking.id}">DELETE</a></td>
							
						</tr>	
						</c:forEach>
						</c:if>
			      </table> 
                 
                 </div>
                
            </div>
		 
     </div>         
 --%>