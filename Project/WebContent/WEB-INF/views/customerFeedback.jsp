<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Parking Management System</title>
</head>
<body>
	<div style="width: 600px; padding-top: 50px; margin-left: 300px;">
		<div class="row myrow">
				<div class="col-md-4">
					<label>Enter Name</label>
				</div>
				<div class="col-md-6">
					<input type="text" id="nameId" name="name" class="form-control" value="${name}">
				</div>
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Enter Email</label>
				</div>
				<div class="col-md-6">
					<input class="form-control" id="emailId" type="text" name="email" value="${email}">
				</div>
			</div>
			
			<div class="row myrow">
				<div class="col-md-4">
					<label>Message</label>
				</div>
				<div class="col-md-6">
					<input class="form-control" type="text" id="messageId" value="${message}" name="message"/>
				</div>
			</div>
			
			<div>
				<center><input class="btn btn-info" style="margin-top: 20px;" type="submit" value="SUBMIT"/></center>
			</div>
			
			<center><h3>${resultMessage}</h3></center>
	</div>
</body>
</html>