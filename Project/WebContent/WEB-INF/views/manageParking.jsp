<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">  
	<div class="col-md-6">
			 
			<%--  <div class="row">
			 		<div class="col-md-4">Select Date</div>
					<div class="col-md-7">
						<input type="date" class="form-control" name="allotedDate" value="${dateValue}" >
				    </div>
				 <!--  <input type="date" id="start" name="trip-start" value="" min="2018-01-01" max="2018-12-31"> -->
			 </div> --%>
			 
			 
			 
			 <div class="row">
					<div class="col-md-4">Select Employee Id </div>
					<div class="col-md-7">
						<select class="form-control" name="employeeId">
						  <c:if test="${not empty employeeList}">
						   <optgroup label="EmpId -- Address">
						    <c:forEach items="${employeeList}" var="employee">
						        <option value="${employee.empId}"  ${employee.empId}== selectedCity ? 'selected' : ''}> ${employee.empId} --- ${employee.name}</option>
		      			    </c:forEach>
		      			   </optgroup>
						  </c:if> 
					  </select>
				 </div>
			</div>  	
			 
			<div class="row">
					<div class="col-md-4">Select Parking Id </div>
					<div class="col-md-7">
						<select class="form-control" name="parkingId" id="parkingId">
						  <c:if test="${not empty parkingList}">
						    <c:forEach items="${parkingList}" var="parking">
						        <option  value="${parking.parkingId}"  ${parking.parkingId}== selectedParkingId ? 'selected' : ''}> ${parking.parkingId} - - - ${parking.address}</option>
						   	</c:forEach>
						  </c:if> 
						</select> 
					</div>
					   
			</div>  
			
			  
				  
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 2 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot2" name="2slot" value="${availableSlotValue}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div>   
					
					<div class="col-md-2">
					   <input type="text" class="form-control" id="rent2" name="2rent">
				   </div> 
			</div>
			
			<div class="row">
			 		<div class="col-md-4">Allotted  Slot 4 Wheeler</div>
					<div class="col-md-3">
					<input type="number" class="form-control" id="slot4" name="4slot" value="${availableSlotValue}" >					 
					</div>
					<div class="col-md-2">Charges Rs:</div> 
					 <div class="col-md-2">
					   <input type="text" class="form-control" id="rent4" name="4rent">
				   </div>    
			</div>
			
			<div class="row">
			 		<div class="col-md-4">
						
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-info form-control" value="${buttonValue}">
					</div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
			</div>
	  </div>
	  
	  
	  <div class="col-md-6" >  
                 
                 <div class="row">
                 <div class="col-md-7">
                	 <input type="search" class="form-control" name="search" placeholder="Search here...">
                 </div>
                                                                                   
                 <div class="col-md-3">
               		<input type="submit" class="btn btn-info form-control" value="SEARCH">
                 </div>
				 
              </div>
                 
                 
              <div class="row">
                 
                  <table  class="table">
						<tr>
							<th>ID</th>
							<th>EMP NAME</th>
							<th>PARKING ADDRESS</th>
							
						</tr>	
						
						<c:if test="${not empty allotedParkingList}">
						<c:forEach var="allotedParking" items="${allotedParkingList}">
						<tr>
							<td>${allotedParking.id}</td>
							<td>${allotedParking.getEmployee().getName()}</td>
							<td>${allotedParking.getParking().getAddress()}</td>
							
						</tr>	
						</c:forEach>
						</c:if>
			      </table> 
                 
                 </div>
                
            </div>
	  
	  
</div>	