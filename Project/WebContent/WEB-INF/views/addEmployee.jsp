<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="row">  
	 <div class="col-md-6">
	        <input type="hidden" class="form-control" name="id" value="${idValue}" >
			 <div class="row">
				  <div class="col-md-4">Enter Employee ID</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="empId" value="${empIdValue}" >
				    </div>
			 </div>
			 
			 <div class="row">
				  <div class="col-md-4">Enter Employee Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="name" value="${empNameValue}" >
				    </div>
			 </div>
			
			<div class="row">
					<div class="col-md-4">Enter Email</div>
					<div class="col-md-7">
						<input type="email" class="form-control" name="emailId" value="${emailValue}">
					</div>
				</div>  
				
			<div class="row">
					<div class="col-md-4">Enter Password</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="password" value="${passwordValue}">
					</div>
				</div> 
			
			
             <div class="row">
					<div class="col-md-4">Enter Employee Address</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="address" value="${addressValue}" >
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-4">Enter Near Landmark</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="landmark" value="${landmarkValue}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-4">Enter Area PIN</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="pin" value="${pinValue}">
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-4">Enter City Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="city" value="${cityValue}">
					</div>
				</div>  
					   
				<div class="row">
					<div class="col-md-4">Enter State Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="state" value="${stateValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-4">Enter Country Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="country" value="${countryValue}">
					</div>
				</div>  
				
				<div class="row">
				    <div class="col-md-4">
						
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-info form-control" value="${buttonValue}">
					</div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
				</div>
     </div>
    
    
    
    	 <div class="col-md-6" >  
                 
                 <div class="row">
                 <div class="col-md-7">
                	 <input type="search" class="form-control" name="search" placeholder="Search here...">
                 </div>
                                                                                   
                 <div class="col-md-3">
               		<input type="submit" class="btn btn-info form-control" value="SEARCH">
                 </div>
				 
              </div>
                 
                 
              <div class="row">
                 
                  <table  class="table">
						<tr>
							<th>EMP ID</th>
							<th>NAME</th>
							<th>CITY</th>
							<th>ACTION</th>
						</tr>	
						
						<c:if test="${not empty employeeList}">
						<c:forEach var="employee" items="${employeeList}">
						<tr>
							<td>${employee.empId}</td>
							<td>${employee.name}</td>
							<td>${employee.city}</td>
						 <td><a href="./getEmployeeUpdateRecord?id=${employee.id}">UPDATE</a> || <a href="./getEmployeeDeleteRecord?id=${employee.id}">DELETE</a></td>
							
						</tr>	
						</c:forEach>
						</c:if>
			      </table> 
                 
                 </div>
                
            </div>
     
</div>




<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
      <div class="row">  
         
          <div class="col-md-6">
			<input type="hidden" class="form-control" name="id" value="${idValue}" >
					  
			    <div class="row">
				  <div class="col-md-5">Enter Parking ID</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="parkingId" value="${parkingIdValue}" >
					</div>
				</div>
				
				
		
				<div class="row">
				
				  <div class="col-md-5">Slot for 2 Wheeler</div>		
					<div class="col-md-3">
					  <select class="form-control" name="employeeId">
							<c:forEach begin="1" end="2000" var="counter">
								<option value="">${counter}</option>
							</c:forEach>
					  </select>
					 </div>
					
					<div class="col-md-2">Charges :</div>	
					
					<div class="col-md-2">
					   <input type="text" class="form-control" name="rent" value="${2rent}" >
				    </div>	
				</div>
			
			
			
				<div class="row">
					<div class="col-md-5">Enter Parking Address</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="address" value="${addressValue}" >
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-5">Enter Near Landmark</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="landmark" value="${landmarkValue}">
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-5">Enter Area PIN</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="pin" value="${pinValue}">
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-5">Enter City Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="city" value="${cityValue}">
					</div>
				</div>  
					   
				<div class="row">
					<div class="col-md-5">Enter State Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="state" value="${stateValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-5">Enter Country Name</div>
					<div class="col-md-7">
						<input type="text" class="form-control" name="country" value="${countryValue}">
					</div>
				</div>  
				
				<div class="row">
					<div class="col-md-5">Select Employee </div>
					<div class="col-md-7">
						<select class="form-control" name="employeeId">
						  <c:if test="${not empty employeeList}">
						    <c:forEach items="${employeeList}" var="employee">
						        <option value="${employee.empId}"  ${employee.empId}== selectedCity ? 'selected' : ''}> ${employee.empId}  ${employee.name}</option>
						    </c:forEach>
						  </c:if> 
						</select>
					</div>
				</div>  
				
				
				
					   
				<div class="row">
				    <div class="col-md-5">
						
					</div>
					<div class="col-md-3">
						<input type="submit" class="btn btn-info form-control" value="${buttonValue}">
					</div>
					<div class="col-md-3">
						<input type="reset" class="btn btn-info form-control" value="RESET">
					</div>
				</div>
			
				<div class="row">
					<div class="col-md-6">
						<h1>${msg}</h1>				
					</div>
				</div>	
			  </div>
			  
			  
			  
			  <div class="col-md-1">
			  	<!-- Gap Between two colomn -->
			  </div>
			  
			 
			  
			  <div class="col-md-5" >  
                 
                 <div class="row">
                 <div class="col-md-7">
                	 <input type="search" class="form-control" name="search" placeholder="Search here...">
                 </div>
                                                                                   
                 <div class="col-md-3">
               		<input type="submit" class="btn btn-info form-control" value="SEARCH">
                 </div>
				 
                 </div>
                 
                 
                 <div class="row">
                 
                  <table  class="table">
						<tr>
							<th>PARKING ID</th>
							<th>CITY</th>
							<th>STATE</th>
							<th>   ACTION</th>
						</tr>	
						
						<c:if test="${not empty parkingList}">
						<c:forEach var="parking" items="${parkingList}">
						<tr>
							<td>${parking.parkingId}</td>
							<td>${parking.city}</td>
							<td>${parking.getEmployee().getName()}</td>
						 <td><a href="./getParkingUpdateRecord?id=${parking.id}">UPDATE</a> || <a href="./getParkingDeleteRecord?id=${parking.id}">DELETE</a></td>
							
						</tr>	
						</c:forEach>
						</c:if>
			      </table> 
                 
                 </div>
                
            </div>
		 
     </div>         
 --%>