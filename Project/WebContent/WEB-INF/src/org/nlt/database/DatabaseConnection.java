package org.nlt.database;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.catalina.SessionListener;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;

public class DatabaseConnection implements ServletContextListener
{
	public static SessionFactory sf=null;
	public static Session ses=null;

	@Override
	public void contextInitialized(ServletContextEvent sce)
	{
	   
		sf=new Configuration().configure("org/nlt/database/hibernate.cfg.xml").buildSessionFactory();
		ses=sf.openSession();
	}
	
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
		if(ses!=null)
		{
			ses.close();
		}
		if(sf!=null)
		{
			sf.close();
		}
	}
	public static Session getDatabaseSession()
	{
		return ses;
	}

}
